const { Builder, By, until } = require('selenium-webdriver')

async function testYouTube() {
  let driver = await new Builder().forBrowser('firefox').build()
  try {
    await driver.get('https://youtube.com/')
    const searchKey = 'мемы'
    await driver.wait(until.titleIs('YouTube'), 1000)
    const search = await driver.findElement(By.xpath('//*[@id="search"]'))
    await search.sendKeys(searchKey)
    const searchButton = await driver.findElement(By.xpath('//*[@id="search-icon-legacy"]'))
    await searchButton.click()
    await driver.wait(until.elementLocated(By.xpath('//*[@id="contents"]/ytd-item-section-renderer')))
    const firstVideoTitle = await driver.findElement(By.xpath('//*[@id="video-title"]/yt-formatted-string')).getText()
    if (firstVideoTitle.toString().toLowerCase().includes(searchKey)) {
      console.log('It is a relevant video. Correct search')
    } else {
      console.log('It is irrelevant video. Incorrect search')
    }
  }
  catch (e) {
    console.log(e)
  }
  finally {
    await driver.quit();
  }
}

testYouTube().then(r => console.log('Test is finished successfully'))
